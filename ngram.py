import os
import random
import sys
from collections import defaultdict

from nltk.tokenize import word_tokenize


def get_ngrams(n: int, tokens: list) -> list:
    """
    Generate n-grams from a list of tokens.

    Args:
        n (int): The order of the n-grams to generate.
        tokens (list): A list of tokens from which to generate n-grams.

    Returns:
        list: A list of n-grams, where each n-gram is represented as a tuple.
    """
    # Adding start tokens
    tokens = (n - 1) * ['<START>'] + tokens

    ngrams = [(tuple([tokens[i - p - 1] for p in reversed(range(n - 1))]), tokens[i])
              for i in range(n - 1, len(tokens))]

    return ngrams


def roulette_wheel(tokens_to_probs: dict):
    """
    Selects a token using the roulette wheel selection method based on probabilities.

    This method selects a token from the given dictionary of tokens mapped to their probabilities
    using the roulette wheel selection method, where the probability of each token determines
    its chance of being selected.

    Args:
        tokens_to_probs (dict): A dictionary where keys are tokens and values are their corresponding probabilities.

    Returns:
        str: The selected token based on its probability.
    """
    rnd = random.uniform(0, 1)

    total_probability = 0
    for token in sorted(tokens_to_probs):
        total_probability += tokens_to_probs[token]
        if total_probability > rnd:
            return token


def read_file(path: str) -> str:
    """
    Read the contents of a file.

    Args:
        path (str): The path to the file to be read.

    Returns:
        str: The contents of the file as a string.
    """
    with open(path, 'r', encoding='utf8') as f:
        text = f.read()

    return text



class NgramModel:
    """
    A class representing an n-gram language model.

    Attributes:
        n (int): The size of the n-grams.
        context (defaultdict): A defaultdict storing contexts and their associated n-grams.
        ngram_counter (dict): A dictionary storing counts of n-grams.
    """

    def __init__(self, n):
        """
        Initialize the NgramModel with the specified n-gram size.

        Args:
            n (int): Size of the n-grams.
        """
        self.n = n
        self.context = defaultdict(list)
        self.ngram_counter = {}

    def update(self, sentence: str) -> None:
        """
        Update the n-gram model with a new sentence.

        This method updates the n-gram model with the given sentence by extracting n-grams,
        updating the n-gram counter, and updating the context.

        Args:
            sentence (str): The input sentence to update the model with.

        Returns:
            None
        """
        # Extract n-grams from the sentence
        ngrams = get_ngrams(self.n, word_tokenize(sentence))

        # Update n-gram counter and context
        for ngram in ngrams:
            # Update n-gram counter
            self.ngram_counter[ngram] = self.ngram_counter.get(ngram, 0.0) + 1.0

            # Update context
            prev_words, target_word = ngram
            self.context[prev_words].append(target_word)

    def get_probability(self, context, token, distinct_tokens):
        """
        Calculate the probability of a token given a context.

        This method calculates the probability of a token given a context using the add-one smoothing technique.

        Args:
            context: The context for the token.
            token (str): The target token.
            distinct_tokens (int): The number of distinct tokens in the vocabulary.

        Returns:
            float: The probability of the token given the context.
        """
        token_count = self.ngram_counter[(context, token)]
        context_len = float(len(self.context[context]))
        result = (token_count + 1) / (context_len + distinct_tokens)

        return result

    def get_random_token(self, context):
        """
        Select a random token given a context using probabilities.

        This method selects a random token from the given context based on the probabilities
        calculated using the `prob` method and employs the roulette wheel selection method.

        Args:
            context: The context for selecting a token.

        Returns:
            str: The selected token.
        """
        tokens_to_probs = {}

        # Get distinct tokens and calculate probabilities for each token
        tokens = self.context[context]
        distinct_tokens = len(set(tokens))
        for token in tokens:
            tokens_to_probs[token] = self.get_probability(context, token, distinct_tokens)

        return roulette_wheel(tokens_to_probs)

    def generate_text(self, token_count: int):
        """
        Generate text using the n-gram model.

        This method generates text of a specified length using the n-gram model by repeatedly
        selecting random tokens based on the context and appending them to the result.

        Args:
            token_count (int): The desired number of tokens in the generated text.

        Returns:
            str: The generated text.
        """
        n = self.n
        queue = ['<START>'] * (n - 1)
        res = []

        for _ in range(token_count):
            # Select a random token based on the current context
            token = self.get_random_token(tuple(queue))
            res.append(token)

            # Update the context queue
            if n > 1:
                queue.pop(0)
                if token == '.':
                    queue = ['<START>'] * (n - 1)
                else:
                    queue.append(token)

        return ' '.join(res)


def create_ngram_model(n: int, text: str) -> NgramModel:
    """
    Create an n-gram language model from the given text.

    Args:
        n (int): The size of the n-grams.
        text (str): The text used to build the model.

    Returns:
        NgramModel: An n-gram language model trained on the provided text.

    """
    model = NgramModel(n)
    for sentence in text.split('.'):
        model.update(sentence + '.')
    return model


if __name__ == "__main__":
    if len(sys.argv) != 4:
        raise ValueError('File path, number of words and window size (in that order)  must be given as a program arguments.')

    file_name = sys.argv[1]
    num_of_words = int(sys.argv[2])
    n = int(sys.argv[3])

    if not os.path.isfile(file_name):
        raise FileNotFoundError()

    text = read_file(file_name)

    model = create_ngram_model(n, text)

    print('\nGenerated text:')
    print(model.generate_text(num_of_words))
