<h1>N-Gram Language Model</h1>

<p>This project implements an N-gram language model in Python. An N-gram model is a probabilistic language model that predicts the next item in a sequence based on the previous items.</p>

<h2>Features</h2>
<ul>
    <li>Supports variable order N-grams.</li>
    <li>Uses add-one(Laplace) smoothing for probability estimation.</li>
    <li>Provides text generation functionality based on the trained model.</li>
</ul>

<h2>Usage</h2>
<p>To use the N-gram model:</p>
<ol>
    <li>Install the required dependencies by running <code>pip install -r requirements.txt</code>.</li>
    <li>Run the <code>main.py</code> script, providing the path to the text file, the number of words to generate, and the N-gram order as command-line arguments.</li>
</ol>

<h2>Dependencies</h2>
<ul>
    <li>Python 3.x</li>
    <li>NLTK library for tokenization (<code>pip install nltk</code>)</li>
</ul>

<h2>Example</h2>
<p>To generate text using a trigram model trained on a text file named <code>corpus.txt</code> and generate 100 words:</p>
<pre><code>python main.py corpus.txt 100 3</code></pre>
